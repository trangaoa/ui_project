import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image, FlatList } from "react-native";
import { ScaledSheet } from 'react-native-size-matters';

const Question3 = ({ navigation, route }) => {
    const [selected, setSelected] = useState(null);
    const [selected2, setSelected2] = useState(null);
    const [counter, setCounter] = useState(route?.params?.timer);
    const [isCorrect, setCorrect] = useState(null);
    const [isChoosed, setChoosed] = useState(false);
    const [count, setCount] = useState(0);
    const [answers, setAnswers] = useState([
        { id: 0, content: 'Tranh sơn dầu', match: 3 },
        { id: 1, content: '1992', match: 6 },
        { id: 2, content: 'Tranh nước', match: 7 },
        { id: 3, content: '1870', match: 0 },
        { id: 4, content: 'Tranh đông hồ', match: 5 },
        { id: 5, content: '1620', match: 4 },
        { id: 6, content: 'Tranh Phục Hưng', match: 1 },
        { id: 7, content: '2000', match: 2 }
    ])

    useEffect(() => {
        if (counter > 0) {
            let interval = setInterval(() => {
                setCounter(lastTimerCount => {
                    lastTimerCount <= 1 && clearInterval(interval)
                    return lastTimerCount - 1
                })
            }, 1000)
            return () => clearInterval(interval)
        } else navigation.navigate("Home", {counter: 0, question: 2})
    }, [counter]);

    useEffect(() => {
        if (isChoosed) {
            console.log("select", selected, "eelld", selected2)
            if (selected?.id == selected2?.match || selected2?.id == selected?.match) {
                setCorrect(true);
                let arr = answers;
                arr[selected?.id].content = '';
                arr[selected2?.id].content = '';
                setAnswers(arr);
                setCount((preValue) => preValue + 2)
            } else setCorrect(false);
            setSelected(null);
            setSelected2(null);
            setChoosed(false)
            let interval = setInterval(() => {
                setCorrect(null);
            }, 1000)
            return () => clearInterval(interval);
        }
    }, [isChoosed])

    const onChooseAnswer = (item) => {
        if (item?.id % 2 == 0) {
            if (selected2) {
                setSelected(item);
                setChoosed(true);
            } else setSelected(item?.id != selected?.id ? item : null)
        } else {
            if (selected) {
                setSelected2(item)
                setChoosed(true);
            } else setSelected2(item?.id != selected?.id ? item : null)
        }
    }

    const convertTime = () => {
        var sec_num = parseInt(counter, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return minutes + ':' + seconds;
    }

    return (
        <View style={styles.container}>
            <View style={styles.viewQuestion}>
                <Text style={styles.txtTimer}>Thời gian: {convertTime()}</Text>
                <Text style={styles.txtAnswer}>Vòng 3:</Text>
                <Text style={styles.txtAnswer}>Nối các mốc thời gian với các sự kiện tương ứng:</Text>
                <FlatList
                    data={answers}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity activeOpacity={0.7} key={index} disabled={item?.content ? false : true}
                                style={item?.content ?
                                    selected?.id == item.id || selected2?.id == item.id ?
                                        [styles.viewSelectedAnswer, { backgroundColor: isChoosed ? isCorrect ? '#18C51F' : '#F62020' : 'rgba(196, 196, 196, 0.5)' }]
                                        : styles.viewAnswer
                                    : styles.viewNo}
                                onPress={() => onChooseAnswer(item)}>
                                <Text style={styles.txtAnswer}>{item.content}</Text>
                            </TouchableOpacity>
                        )
                    }
                    }
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={2}
                />
            </View>
            <View style={styles.viewRow}>
                <Image
                    source={{ uri: 'https://images.all-free-download.com/images/graphiclarge/vietnam_traditional_clothes_template_cute_cartoon_girl_icon_6839472.jpg' }}
                    style={{ width: 200, height: 300 }}
                />
                {count == 8 ?
                    <TouchableOpacity onPress={() => navigation.navigate('Question4', { timer: counter })}>
                        <Image
                            source={require('../../../assets/Home/next.png')}
                            style={{ width: 50, height: 50, marginRight: 50 }}
                        />
                    </TouchableOpacity>
                    : <></>
                }
            </View>
        </View >
    );
};

export default Question3;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    viewQuestion: {
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        marginVertical: 20,
        marginHorizontal: 15,
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 30,
        borderRadius: 10,
        flex: 1
    },
    image: {
        height: 170,
        width: 300,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginVertical: 10
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    viewAnswer: {
        flex: 1,
        borderWidth: 0.5,
        marginHorizontal: 5,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        borderRadius: 20
    },
    viewNo: {
        flex: 1,
        height: 50,
        marginTop: 15,
        justifyContent: 'center',
        marginHorizontal: 5
    },
    viewSelectedAnswer: {
        flex: 1,
        height: 50,
        marginHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        borderRadius: 20,
        backgroundColor: 'rgba(196, 196, 196, 0.5)'
    },
    txtTimer: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 10
    },
    txtAnswer: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 16,
        color: 'black'
    }
});
