import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image, FlatList } from "react-native";
import { ScaledSheet } from 'react-native-size-matters';

const Question2 = ({ navigation, route }) => {
    const [selected, setSelected] = useState(null);
    const [counter, setCounter] = useState(route?.params?.timer);
    let answers = [
        { id: 1, content: 'Tranh sơn dầu' },
        { id: 2, content: 'Tranh đông hồ' },
        { id: 3, content: 'Tranh nước' },
        { id: 4, content: 'Tranh Phục Hưng' },
    ]

    useEffect(() => {
        if (counter > 0) {
            let interval = setInterval(() => {
                setCounter(lastTimerCount => {
                    lastTimerCount <= 1 && clearInterval(interval)
                    return lastTimerCount - 1
                })
            }, 1000)
            return () => clearInterval(interval)
        } else navigation.navigate("Home", {counter: 0, question: 1})
    }, [counter]);

    const onChooseAnswer = (item) => {
        setSelected(item)
    }

    const convertTime = () => {
        var sec_num = parseInt(counter, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return minutes + ':' + seconds;
    }

    return (
        <View style={styles.container}>
            <View style={styles.viewQuestion}>
                <Text style={styles.txtTimer}>Thời gian: {convertTime()}</Text>
                <Text style={styles.txtAnswer}>Vòng 2:</Text>
                <Image
                    source={{ uri: 'https://lh3.googleusercontent.com/proxy/22unCPqdso0CaOED52unial6zNgR4su8ZFvGXVR0o88T0bHyXWQGLvxQHTUf-cnxscXWKXXN3tv4rcvnronnSYlc3hzHCXet_JIKQ9uc4clmmmhjsEE9njDlKj2CxpA' }}
                    style={styles.image}
                />
                <Text style={styles.txtAnswer}>Bức tranh trên thuộc thể loại tranh nào?</Text>
                <FlatList
                    data={answers}
                    renderItem={({ item, index }) => {
                        let type = item.id == 1 ? 'A' : (item.id == 2 ? 'B' : item.id == 3 ? 'C' : 'D');
                        return (
                            <TouchableOpacity disabled={selected?.id == 2 ? true : false} activeOpacity={0.7} key={index}
                                style={selected?.id == item?.id ? [styles.viewSelectedAnswer, { backgroundColor: selected?.id == 2 ? '#18C51F' : '#F62020' }] : styles.viewAnswer}
                                onPress={() => onChooseAnswer(item)}>
                                <Text style={[styles.txtAnswer, { color: selected?.id == item.id ? 'white' : 'black' }]}>{type}. {item.content}</Text>
                            </TouchableOpacity>
                        )
                    }
                    }
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={2}
                />
            </View>
            <View style={styles.viewRow}>
                <Image
                    source={{ uri: 'https://thuvienvector.com/upload/images/items/vector-chibi-be-trai-mac-ao-dai-truyen-thong-1657.webp' }}
                    style={{ width: 200, height: 300 }}
                />
                {selected?.id == 2 ?
                    <TouchableOpacity onPress={() => navigation.navigate('Question3', {timer: counter})}>
                        <Image
                            source={require('../../../assets/Home/next.png')}
                            style={{ width: 50, height: 50, marginRight: 50 }}
                        />
                    </TouchableOpacity>
                    : <></>
                }
            </View>
        </View >
    );
};

export default Question2;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    viewQuestion: {
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        marginTop: 20,
        marginHorizontal: 15,
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 30,
        borderRadius: 10
    },
    image: {
        height: 170,
        width: 300,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginVertical: 10
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    viewAnswer: {
        flex: 1,
        borderWidth: 0.5,
        marginHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center',
        marginTop: 15,
        borderRadius: 20
    },
    viewSelectedAnswer: {
        flex: 1,
        marginHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center',
        marginTop: 15,
        borderRadius: 20
    },
    txtTimer: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 10
    },
    txtAnswer: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 16
    }
});