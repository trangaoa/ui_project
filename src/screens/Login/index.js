import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { ScaledSheet } from 'react-native-size-matters';
import Scan from "../../components/Scan";

const Login = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>LOGO</Text>
            <Text style={styles.txtContent}>Chào mừng bạn đến với bảo tàng văn hóa các dân tộc Việt Nam</Text>
            <Scan />
            <Text style={styles.txtQR}>Vui lòng quét mã QR để tham gia trò chơi</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Question1')}>
                <Image source={require('../../assets/Login/next.png')} style={styles.icoNext} />
            </TouchableOpacity>
        </View >
    );
};

export default Login;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FCE5E5'
    },
    title: {
        fontSize: 48,
        color: '#AE1C1C',
        marginTop: 60,
        fontFamily: 'Philosopher-Regular'
    },
    txtContent: {
        fontSize: 18,
        marginHorizontal: 30,
        textAlign: 'center',
        color: 'black',
        marginVertical: 20,
        fontFamily: 'Philosopher-Regular'
    },
    txtQR: {
        fontSize: 14,
        color: 'rgba(0, 0, 0, 0.44)',
        fontFamily: 'Baloo Regular 400',
        marginTop: 30
    },
    icoNext: {
        width: 50,
        height: 50,
        marginTop: 10
    }
});
