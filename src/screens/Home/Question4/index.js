import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image, TextInput, ScrollView } from "react-native";
import { ScaledSheet } from 'react-native-size-matters';

const Question4 = ({ navigation, route }) => {
    const [input, setInput] = useState(null);
    const [result, setResult] = useState(null);
    const [counter, setCounter] = useState(route?.params?.timer);

    useEffect(() => {
        if (counter > 0) {
            let interval = setInterval(() => {
                setCounter(lastTimerCount => {
                    lastTimerCount <= 1 && clearInterval(interval)
                    return lastTimerCount - 1
                })
            }, 1000)
            return () => clearInterval(interval)
        } else navigation.navigate("Home", { counter: 0, question: 3 })
    }, [counter]);

    const convertTime = () => {
        var sec_num = parseInt(counter, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return minutes + ':' + seconds;
    }

    const onSubmit = () => {
        if (input == 'd') {
            setResult(true)
        } else {
            setResult(false)
        }
    }

    return (
        <ScrollView style={styles.container}>
            <View style={styles.viewQuestion}>
                <Text style={styles.txtTimer}>Thời gian: {convertTime()}</Text>
                <Text style={styles.txtAnswer}>Vòng 4: Điền từ còn thiếu</Text>
                <Image
                    source={{ uri: 'https://lh3.googleusercontent.com/proxy/22unCPqdso0CaOED52unial6zNgR4su8ZFvGXVR0o88T0bHyXWQGLvxQHTUf-cnxscXWKXXN3tv4rcvnronnSYlc3hzHCXet_JIKQ9uc4clmmmhjsEE9njDlKj2CxpA' }}
                    style={styles.image}
                />
                <Text style={styles.txtAnswer}>Tranh ... là một dòng tranh dân gian Việt Nam với xuất xứ từ làng Đông Hồ (xã Song Hồ, huyện Thuận Thành, tỉnh Bắc Ninh)</Text>
                <TextInput
                    placeholder={'Điền từ còn thiếu vào chỗ trống'}
                    value={input}
                    onChangeText={setInput}
                    style={styles.txtInput}
                />
                <View style={styles.viewRow}>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => onSubmit()} disabled={result || !input}>
                        <Text style={input ? styles.btnActive : styles.btnSubmit}>Trả lời</Text>
                    </TouchableOpacity>
                    {result != null ? <Text style={{ color: result ? '#18C51F' : '#F62020' }}>{result ? "Bạn đã trả lời đúng câu hỏi" : "Bạn đã trả lời sai câu hỏi"}</Text> : <></>}
                </View>
            </View>
            <View style={styles.viewRow}>
                <Image
                    source={{ uri: 'https://thuvienvector.com/upload/images/items/vector-chibi-be-trai-mac-ao-dai-truyen-thong-1657.webp' }}
                    style={{ width: 200, height: 300 }}
                />
                {result ?
                    <TouchableOpacity onPress={() => navigation.navigate('Home', { counter: counter, question: 4 })}>
                        <Image
                            source={require('../../../assets/Home/next.png')}
                            style={{ width: 50, height: 50, marginRight: 50 }}
                        />
                    </TouchableOpacity>
                    : <></>
                }
            </View>
        </ScrollView >
    );
};

export default Question4;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    viewQuestion: {
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        marginVertical: 20,
        marginHorizontal: 15,
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 15,
        borderRadius: 10,
        flex: 1
    },
    image: {
        height: 170,
        width: 300,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginVertical: 10
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    viewAnswer: {
        flex: 1,
        borderWidth: 0.5,
        marginHorizontal: 5,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        borderRadius: 20
    },
    viewNo: {
        flex: 1,
        height: 50,
        marginTop: 15,
        justifyContent: 'center',
        marginHorizontal: 5
    },
    viewSelectedAnswer: {
        flex: 1,
        height: 50,
        marginHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        borderRadius: 20,
        backgroundColor: 'rgba(196, 196, 196, 0.5)'
    },
    txtTimer: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 10
    },
    txtAnswer: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 16,
        color: 'black'
    },
    txtInput: {
        flex: 1,
        marginVertical: 10,
        paddingVertical: 6,
        fontSize: 16,
        borderRadius: 10,
        borderWidth: 0.5,
        paddingHorizontal: 10
    },
    btnSubmit: {
        backgroundColor: 'rgba(196, 196, 196, 0.5)',
        width: 80,
        textAlign: 'center',
        fontSize: 16,
        paddingVertical: 8,
        borderRadius: 10
    },
    btnActive: {
        backgroundColor: '#18C51F',
        color: 'white',
        width: 80,
        textAlign: 'center',
        fontSize: 16,
        paddingVertical: 8,
        borderRadius: 10

    }
});
