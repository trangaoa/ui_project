import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// List screen
import Login from '../screens/Login';
import Home from '../screens/Home';
import Question1 from '../screens/Home/Question1';
import Question2 from '../screens/Home/Question2';
import Question3 from '../screens/Home/Question3';
import Question4 from '../screens/Home/Question4';
import SplashScreen from '../screens/SplashScreen';

const Stack = createStackNavigator();

const Routes = () => {
    // Create stack navigation
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={'SplashScreen'}>
                <Stack.Screen name="SplashScreen" component={SplashScreen} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Question1" component={Question1} />
                <Stack.Screen name="Question2" component={Question2} />
                <Stack.Screen name="Question3" component={Question3} />
                <Stack.Screen name="Question4" component={Question4} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Routes;
