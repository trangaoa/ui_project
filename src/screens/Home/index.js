import React from "react";
import { View, Text, Image } from "react-native";
import { ScaledSheet } from 'react-native-size-matters';

const Home = ({ navigation, route }) => {
    let counter = route?.params?.counter;
    let question = route?.params?.question;

    const convertTime = () => {
        var sec_num = parseInt(1000 - counter, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return minutes + ':' + seconds;
    }

    return (
        <View style={styles.container}>
            <Image
                source={{ uri: 'https://thumbs.dreamstime.com/z/children-traditional-vietnamese-clothes-hand-drawn-vector-illustration-cute-boy-girl-ao-dai-isolated-white-flat-162655199.jpg' }}
                style={styles.image}
            />
            <View>
                <Text style={styles.txtCong}>Chúc mừng bạn vượt qua {question == 4 ? 'tất cả' : question} câu hỏi và hoàn thành trò chơi</Text>
                <View style={styles.viewRow}>
                    <View style={styles.viewFlex}>
                        <Text style={styles.txtType}>Thời gian:</Text>
                        <Text style={styles.txtNumber}>{convertTime()}</Text>
                    </View>
                    <View style={{ width: 0.75, height: '60%', backgroundColor: 'black' }} />
                    <View style={styles.viewFlex}>
                        <Text style={styles.txtType}>Điểm số:</Text>
                        <Text style={styles.txtNumber}>100</Text>
                    </View>
                </View>
            </View>
            <View style={{ width: '100%', height: 0.5, backgroundColor: 'black' }} />
            <Image
                source={{ uri: 'https://vi.qr-code-generator.com/wp-content/themes/qr/new_structure/markets/basic_market/generator/dist/generator/assets/images/websiteQRCode_noFrame.png' }}
                style={styles.imgQR}
            />

            <Text style={styles.txtBox}>Vui lòng ra cửa buổi triển lãm để đổi các phần thưởng hấp dẫn</Text>
        </View >
    );
};

export default Home;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    image: {
        height: 250,
        width: 375,
        alignSelf: 'center',
        marginVertical: 10,
        resizeMode: 'cover'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 15
    },
    viewFlex: {
        flex: 1,
        alignItems: 'center'
    },
    txtCong: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 24,
        textAlign: 'center',
        marginHorizontal: 20
    },
    txtType: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 18,
        textAlign: 'center'
    },
    txtNumber: {
        fontFamily: 'Philosopher-Regular',
        fontSize: 40,
        color: '#0A9710'
    },
    txtBox: {
        fontFamily: 'Baloo Regular 400',
        fontSize: 16,
        color: 'rgba(0, 0, 0, 0.44)',
        textAlign: 'center',
        // position: 'absolute',
        // bottom: 40,
        marginHorizontal: 60
    },
    imgQR: {
        width: 250,
        height: 250,
        alignSelf: 'center'
    }
});
