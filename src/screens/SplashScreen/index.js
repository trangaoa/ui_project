import React from "react";
import { useEffect } from "react";
import { View, ActivityIndicator, ImageBackground } from "react-native";
import { ScaledSheet } from 'react-native-size-matters';
// import AsyncStorage from '@react-native-async-storage/async-storage';

const isLoggedIn = async () => {
    // try {
    //     const respond = await AsyncStorage.getItem('@save_token');
    //     console.log('Respond:', respond);
    //     if (respond !== null) { return true; }
    //     else return false;
    // } catch (e) {
    //     console.log(e);
    //     return false;
    // }
}

const SplashScreen = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            const handle = async () => {
                const token = await isLoggedIn();
                if (token) {
                    navigation.navigate('Home');
                } else {
                    navigation.navigate('Login');
                }
               
            };
            handle();
        }, 5000);
    }, []); 

    return (
        <View style={styles.container}>
            <ImageBackground source={require('../../assets/SplashScreen/background.png')} style={styles.splashScreen} resizeMode={'stretch'}>
                <ActivityIndicator size="large" color="white" style={{marginTop: 400}} />
            </ImageBackground>
        </View >
    );
};

export default SplashScreen;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    splashScreen: {
        width: '100%',
        height: '100%'
    }
});
