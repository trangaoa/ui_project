import React from 'react';
import Routes from './src/routes';
const App = () => {
  return (
    <React.Fragment>
      <Routes />
    </React.Fragment>
  )
}

export default App;